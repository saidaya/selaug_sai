package flipkartPages;

import java.awt.Window;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.FkMethods;

public class ItemPage extends FkMethods {

public ItemPage() {
	PageFactory.initElements(driver, this);
}
public ItemPage windowHandle() {
	Set<String> allwindows = driver.getWindowHandles();
	List<String> list = new ArrayList<String>();
	list.addAll(allwindows);
	driver.switchTo().window(list.get(1));
	System.out.println(driver.getTitle());
	return this;
}

@FindBy(xpath="//button[text()='ADD TO CART']")
WebElement ac;
public CartPage addCart() {
	ac.click();
	return new CartPage();
}
}
