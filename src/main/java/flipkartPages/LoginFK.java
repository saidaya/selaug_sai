package flipkartPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.FkMethods;
import wdMethods.SeMethods;

public class LoginFK extends FkMethods{
	
	public LoginFK() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="//input[@type='text']")
	WebElement search;
	public LoginFK searchItem() {
		search.sendKeys("fastrack");
		return this;
		}
	
	@FindBy(xpath="//button[@type='submit']")
	WebElement sbtn;
	public Fastrackpage clickSbtn() {
		sbtn.click();
		return new Fastrackpage();
		}
}
