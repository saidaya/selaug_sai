package flipkartPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.FkMethods;

public class Fastrackpage extends FkMethods {

	public Fastrackpage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//img[@alt='Fastrack NG38003PP08 Tees Watch  - For Men & Women']")
	WebElement item;
	public ItemPage selectItem() {
		item.click();
		return new ItemPage();
		
	}
}
