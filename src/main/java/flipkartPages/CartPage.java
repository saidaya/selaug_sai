package flipkartPages;

import org.openqa.selenium.support.PageFactory;

import wdMethods.FkMethods;

public class CartPage extends FkMethods{
	
	public CartPage() {
		PageFactory.initElements(driver, this);
	}

	public CartPage takeSS() {
		takeSnap();
		return this;
	}
}
