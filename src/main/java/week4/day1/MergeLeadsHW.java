package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLeadsHW {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		Thread.sleep(3000);
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("//table[@class ='twoColumnForm']//img").click();
		
		Set <String> newwindow = driver.getWindowHandles();
		List <String> allwindows = new ArrayList<String>();
		allwindows.addAll(newwindow);
	
		driver.switchTo().window(allwindows.get(1));
		System.out.println(driver.getTitle());
		driver.findElementByName("id").sendKeys("10228");
		driver.findElementByXPath("//button[@type='button']").click();
		
		Thread.sleep(2000);
		
		WebElement table = driver.findElementByXPath("//table[@cellspacing = '0']");
		List<WebElement> row = driver.findElements(By.tagName("tr"));
		WebElement firstrow = row.get(0);
		List<WebElement> tabledata = firstrow.findElements(By.xpath("//table[@class='x-grid3-row-table']//a"));
		String td1 = tabledata.get(0).getText();
		System.out.println(td1);
		WebElement td= tabledata.get(0);
		td.click();
		
		Thread.sleep(2000);
		
		driver.switchTo().window(allwindows.get(0));
		System.out.println(driver.getTitle());
		driver.findElementByXPath("//table[@class ='twoColumnForm']//img").click();
		
		Thread.sleep(3000);
		
		Set <String> newwindow1 = driver.getWindowHandles();
		List <String> allwindows1 = new ArrayList<String>();
		allwindows1.addAll(newwindow1);
		driver.switchTo().window(allwindows1.get(1));
		System.out.println(driver.getTitle());
		driver.findElementByName("id").sendKeys("10231");
		driver.findElementByXPath("//button[@type='button']").click();
		
		Thread.sleep(2000);
		
		WebElement table1 = driver.findElementByXPath("//table[@cellspacing = '0']");
		List<WebElement> row1 = driver.findElements(By.tagName("tr"));
		WebElement firstrow1 = row1.get(0);
		List<WebElement> tabledata1 = firstrow1.findElements(By.xpath("//table[@class='x-grid3-row-table']//a"));
		String td2 = tabledata1.get(0).getText();
		System.out.println(td2);
		WebElement td3= tabledata1.get(0);
		Thread.sleep(2000);
		td3.click();
		
		Thread.sleep(2000);		
		driver.switchTo().window(allwindows.get(0));
		driver.findElementByLinkText("Merge").click();
		Alert palert = driver.switchTo().alert();
		palert.accept();
		
	
		
		
	}

}
