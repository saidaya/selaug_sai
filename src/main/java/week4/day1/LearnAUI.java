package week4.day1;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnAUI {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://jqueryui.com/draggable/");
		
		//Enter into frame
		WebElement frame = driver.findElementByClassName("demo-frame");
		driver.switchTo().frame(frame);
		Actions builder = new Actions(driver);
		WebElement drag = driver.findElementById("draggable");
		System.out.println(drag.getLocation());
		//File src = driver.getScreenshotAs(OutputType.FILE);
		//File des = new File("./snaps/img1.png");
		//FileUtils.copyFile(src, des);
		builder.dragAndDropBy(drag, 200, 200).perform();
		//File src1 = driver.getScreenshotAs(OutputType.FILE);
		//File des1 = new File("./snaps/img2.png");
		//FileUtils.copyFile(src1, des1);

	}
}
