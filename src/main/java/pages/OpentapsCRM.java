package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class OpentapsCRM extends ProjectMethods{
	
	public OpentapsCRM() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name="ownershipEnumId")
	WebElement eleMarket;
	public OpentapsCRM locateElement() {
		selectDropDownUsingIndex(eleMarket, 2);
		return this;
		
	}
	
	@FindBy(xpath="//input[@value='Update']")
	WebElement update;
	public ViewLeadELss clickElement() {
		click(update);
		return new ViewLeadELss();
		
	}

}
