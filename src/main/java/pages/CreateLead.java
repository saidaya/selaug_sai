package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {
	
	public CreateLead()  {
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(id = "createLeadForm_companyName")
	WebElement cName;
	@And("Enter Company Name as (.*)")
	public CreateLead typeCName(String data) {
		type(cName,data);
		return this;	
	}
	@FindBy(id = "createLeadForm_firstName")
	WebElement fName;
	@And("Enter First Name as (.*)")
	public CreateLead typeFName(String data) {
		type(fName,data);
		return this;
	}
	@FindBy(id = "createLeadForm_lastName")
	WebElement lName;
	@And("Enter Last Name as (.*)")
	public CreateLead typelName(String data) {
		type(lName,data);
		return this;
	}
	
	@FindBy(name = "dataSourceId")
	WebElement eleSource;
	@And("Select Source")
	public CreateLead selectSource() {
		selectDropDownUsingText(eleSource,"Employee");
		return this;

}
	@FindBy(name = "submitButton")
	WebElement sButton;
	@And("Click Create Lead button")
	public ViewLead clickSButton() {
		click(sButton);
		return new ViewLead();
	}
}
