package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeadsEL extends ProjectMethods{
	
	public MyLeadsEL() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText="Find Leads")
	WebElement eleFL;
	public FindLeadsEL clickFL1() {
		click(eleFL);
		return new FindLeadsEL();
		
	}

}
