package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods{

	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="username")
	WebElement eleUN;
	
	@And("Enter UserName as (.*)")
	public LoginPage enterUN(String data) {
		type(eleUN,data);
		return this;
	}
	@FindBy(id="password")
	WebElement elePW;
	@And("Enter Password as (.*)")
	public LoginPage enterPW(String data) {
		type(elePW,data);
		return this;
	}
	@FindBy(className="decorativeSubmit1")
	WebElement eleLB;
	@And("Click Login")
	public CRMPage clickLB() throws InterruptedException {
		Thread.sleep(3000);
		click(eleLB);
		
		return new CRMPage();
	}
}
