package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeads extends ProjectMethods{
	
	public FindLeads() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//span[text()='Phone']")
	WebElement ph;
	public FindLeads clickPhone() {
		click(ph);
		return this;
		
	}
	
	@FindBy(name="phoneCountryCode")
	WebElement ccode;
	public FindLeads typeCode(String data) {
		type(ccode,data);
		return this;
		
	}
	@FindBy(name="phoneNumber")
	WebElement phno;
	public FindLeads typePhno(String data) {
		type(phno,data);
		return this;
		
	}
	@FindBy(xpath="//button[text()='Find Leads']")
	WebElement bFL;
	public FindLeads clickbFL() {
		click(bFL);
		return this;
		
	}
	
	public ViewLead1 selectLead() {
		
		WebElement table = driver.findElementByXPath("(//table[@cellspacing='0'])[20]");
		List<WebElement> row = driver.findElementsByTagName("tr");
		WebElement firstrow = row.get(1);
		List<WebElement>tabledata = firstrow.findElements(By.xpath("(//table[@cellspacing='0']//a)[33]"));
		WebElement td = tabledata.get(0);
		String text = td.getText();
		System.out.println(text);
		td.click();
		return new ViewLead1();
	}

}
