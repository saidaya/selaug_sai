package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeads1 extends ProjectMethods{
	
	public MyLeads1() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText="Find Leads")
	WebElement eleFL;
	public FindLeads clickFL() {
		click(eleFL);
		return new FindLeads();
		
	}

}
