package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class CRMPage extends ProjectMethods {

	public CRMPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(linkText="CRM/SFA")
	WebElement eleCRM;
	@And("click on CRMSFA")
	public MyHomePage clickCRM() {
		click(eleCRM);
		return new MyHomePage();
	}
}
