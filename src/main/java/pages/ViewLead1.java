package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLead1 extends ProjectMethods {
	
	public ViewLead1() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText="Delete")
	WebElement delete;
	public MyLeads1 deleteLead() {
		click(delete);
		return new MyLeads1();
	}

}
