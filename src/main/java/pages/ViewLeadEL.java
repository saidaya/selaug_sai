package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeadEL extends ProjectMethods {
	
	public ViewLeadEL() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText="Edit")
	WebElement edit;
	public OpentapsCRM editLead() {
		click(edit);
		return new OpentapsCRM();
	}

}
