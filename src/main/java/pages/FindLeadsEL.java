package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsEL extends ProjectMethods{
	
	public FindLeadsEL() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="(//input[@name='firstName'])[3]")
	WebElement fname;
	public FindLeadsEL findFN() {
		type(fname,"");
		return this;
		
	}
	
	@FindBy(xpath="//button[text()='Find Leads']")
	WebElement bFL;
	public FindLeadsEL clickbFL() {
		click(bFL);
		return this;
		
	}
	
	public ViewLeadEL selectLead() throws InterruptedException {
		
		Thread.sleep(3000);
		WebElement table = driver.findElementByXPath("(//table[@cellspacing='0'])[20]");
		List<WebElement> row = driver.findElementsByTagName("tr");
		WebElement firstrow = row.get(1);
		List<WebElement>tabledata = firstrow.findElements(By.xpath("(//table[@cellspacing='0']//a)[33]"));
		WebElement td = tabledata.get(0);
		String text = td.getText();
		System.out.println(text);
		td.click();
		return new ViewLeadEL();
	}

}
