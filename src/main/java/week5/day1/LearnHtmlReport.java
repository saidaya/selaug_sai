package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnHtmlReport {

	public static void main(String[] args) throws IOException {
		
		//to create html file
		
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		
		//to convert to writable mode
		
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		//TC level 
		
		ExtentTest test = extent.createTest("TC_002_CreateLead","Create a Lead in LeafTaps");
		test.assignCategory("Smoke");
		test.assignAuthor("Sai");
		
		// Step level status
		
		test.pass("Browser lauched Successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.pass("UN entered successfully");
		test.pass("PW entered successfully");
		test.fail("Logged in ");
		extent.flush();
	}
	

}
