package week3.day1;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Irctc {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		
		// Personal Details
		
		
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Sai");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Dayanandh");
		driver.findElementById("userRegistrationForm:gender:0").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		WebElement date = driver.findElementById("userRegistrationForm:dobDay");
		Select dobdd1 = new Select (date);
		dobdd1.selectByIndex(0);
		WebElement month = driver.findElementById("userRegistrationForm:dobMonth");
		Select dobdd2 = new Select (month);
		List <WebElement> options = dobdd2.getOptions();
		int size = options.size();
		dobdd2.selectByIndex(size-5);
		WebElement year = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select dobdd3 = new Select (year);
		dobdd3.selectByValue("1991");
		WebElement occupation = driver.findElementById("userRegistrationForm:occupation");
		Select occdd = new Select (occupation);
		occdd.selectByVisibleText("Public");
		WebElement Country = driver.findElementById("userRegistrationForm:countries");
		Select countrydd = new Select (Country);
		countrydd.selectByIndex(1);		
		driver.findElementById("userRegistrationForm:email").sendKeys("abc@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("123456789");
		WebElement nation = driver.findElementById("userRegistrationForm:nationalityId");
		Select nationdd = new Select (nation);
		nationdd.selectByIndex(1);
		//ResidentiL Address
		
		driver.findElementById("userRegistrationForm:address").sendKeys("no.1 abc lane");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600091",Keys.TAB);
		Thread.sleep(3000);
		WebElement city = driver.findElementById("userRegistrationForm:cityName");
		Select citydd = new Select (city);
		citydd.selectByVisibleText("Kanchipuram");
		Thread.sleep(3000);
		WebElement PO = driver.findElementById("userRegistrationForm:postofficeName");
		Select podd = new Select (PO);
		podd.selectByIndex(1);
		
	}

}
