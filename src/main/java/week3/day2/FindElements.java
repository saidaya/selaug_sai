package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindElements {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/table.html");
		List <WebElement> cbs = driver.findElementsByXPath("//input[@type = 'checkbox']");
		System.out.println(cbs.size());
		WebElement third = cbs.get(2);
		third.click();
		
		
		
		WebElement table = driver.findElementByXPath("//table[@cellspacing = '0']");
		List<WebElement> row = driver.findElements(By.tagName("tr"));
		WebElement secondrow = row.get(1);
		List<WebElement> tabledata = secondrow.findElements(By.tagName("td"));
		String td2 = tabledata.get(1).getText();
		System.out.println(td2);
		WebElement td = tabledata.get(2);
		td.click();
		
		
		
		
	}

}
