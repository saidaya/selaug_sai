package testcases;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class Facebook extends SeMethods{
	
	@BeforeSuite
	public void beforeSuite() {
		beginResult();
	}
	@BeforeClass
	public void beforeClass() {
		startTestCase();
		
	}
	@BeforeTest
	public void setdata() {
		testCaseName ="TC_001_Facebook";
		testCaseDesc="check req";
		author = "Sai";
		category = "Smoke";
	}
	
	@Test
	
	public void loginFB() throws InterruptedException {
		
		startApp("chrome","https://www.facebook.com/");
		WebElement eleUN = locateElement("id","email");
		type(eleUN,"saiahilan007@gmail.com");
		WebElement elePW = locateElement("id","pass");
		type(elePW,"saiahilan#42");
		WebElement login = locateElement("xpath","//input[@type='submit']");
		click(login);
		Thread.sleep(2000);
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE).perform();
		//driver=new ChromeDriver(options);
		//dismissAlert();
		WebElement search = locateElement("xpath","//input[@name='q']");
		type(search,"TestLeaf");
		WebElement searchbtn = locateElement("xpath","//button[@type='submit']");
		click(searchbtn);
		Thread.sleep(3000);
		WebElement tl = locateElement("linkText","TestLeaf");
		//String tleaf = tl.getText();
		if(getText(tl).equals("TestLeaf")) {
			System.out.println(getText(tl));
		}else {
			System.out.println("not present");
		}
		Thread.sleep(3000);
		WebElement like = locateElement("xpath","(//button[@type='submit'])[2]");
		//String tlike = tl.getText();
		if(getText(like).contains("Like")) {
			System.out.println(getText(like));
			click(like);
		}else if(getText(like).contains("Liked")) {
			System.out.println("Already Liked");
		}
		click(tl);
		String title = driver.getTitle();
		System.out.println(title);
		WebElement nooflikes = locateElement("xpath","//div[text()[contains(.,'people')]]");
		System.out.println(getText(nooflikes));
	}
	
	@AfterSuite
	public void afterSuite() {
		endResult();
	}

}
