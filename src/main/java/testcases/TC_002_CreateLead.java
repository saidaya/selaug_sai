package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC_002_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setdata() {
		testCaseName ="TC_002_CreateLead";
		testCaseDesc="Create a new Lead";
		author = "Sai";
		category = "Smoke";
	}

	@Test 
public void Login() throws InterruptedException {	
	
		WebElement eleCL = locateElement("linkText","Create Lead");
		click(eleCL);
		WebElement eleCompany = locateElement("id","createLeadForm_companyName");
		type(eleCompany,"TestLeaf");
		WebElement eleFirst = locateElement("id","createLeadForm_firstName");
		type(eleFirst,"Test");
		WebElement eleLast = locateElement("id","createLeadForm_lastName");
		type(eleLast,"Test1");
		WebElement eleSource = locateElement("name","dataSourceId");
		selectDropDownUsingText(eleSource, "Employee");
		WebElement eleMarket = locateElement("name","marketingCampaignId");
		selectDropDownUsingIndex(eleMarket, 2);
		WebElement eleCLb = locateElement("name","submitButton");
		//selectDropDownUsingIndex(eleMarket, 2);
		click(eleCLb);
		//closeBrowser();
		
		
	}

}
