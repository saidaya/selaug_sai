package testcases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC_003_EditLead extends ProjectMethods{
	
	@BeforeTest
	public void setdata() {
		testCaseName ="TC_003_EditLead";
		testCaseDesc="Edit Lead";
		author = "Sai";
		category = "Smoke";
	}
	
	@Test
	public void Login() throws InterruptedException {	
			WebElement eleCL = locateElement("linkText","Create Lead");
			click(eleCL);
			WebElement eleFL = locateElement("linkText","Find Leads");
			click(eleFL);
			WebElement elePhone = locateElement("xpath","(//input[@name='firstName'])[3]");
			elePhone.sendKeys("");
			WebElement eleFlb = locateElement("xpath","//button[text()='Find Leads']");
			click(eleFlb);
			Thread.sleep(3000);
			WebElement table = driver.findElementByXPath("(//table[@cellspacing='0'])[20]");
			List<WebElement> row = driver.findElementsByTagName("tr");
			WebElement firstrow = row.get(1);
			List<WebElement>tabledata = firstrow.findElements(By.xpath("(//table[@cellspacing='0']//a)[33]"));
			WebElement td = tabledata.get(0);
			String text = td.getText();
			System.out.println(text);
			td.click();
			WebElement edit = locateElement("linkText","Edit");
			click(edit);
			WebElement eleMarket = locateElement("name","ownershipEnumId");
			selectDropDownUsingIndex(eleMarket, 2);
			WebElement update = locateElement("xpath","//input[@value='Update']");
			click(update);

}
}
