package newtestcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC_002_CreateLead_POM extends ProjectMethods{
	@BeforeTest(groups= {"common"})
	public void setdata() {
		testCaseName ="TC_002_CreateLead_POM";
		testCaseDesc="Create a new Lead";
		author = "Sai";
		category = "Smoke";
		excelFileName ="CLPOM";
	}

	@Test (groups= {"common"},dataProvider="fetchData")
public void createLead(String cname,String fname, String lname) throws InterruptedException {	
	
		new MyHomePage()
		.clickLeads()
		.clickCL()
		.typeCName(cname)
		.typeFName(fname)
		.typelName(lname)
		.selectSource()
		.clickSButton()
		.verifyText(fname);
		
	}

}
