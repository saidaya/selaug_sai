package programs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MaxMin {

	public static void main(String[] args) {
		
		int a[]= {11,13,3,7,18,8,9};
		List<Integer> myList = new ArrayList<Integer>();
		for (Integer eachnum :  a) {
		 myList.add(eachnum);
		}
		System.out.println(myList);
		Collections.sort(myList);
		
		System.out.println("Minimum value is"+myList.get(0));
		System.out.println("Maximum value is"+myList.get(myList.size()-1));

	}

}
