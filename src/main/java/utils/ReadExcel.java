package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static Object[][] getExcelData(String fileName) throws IOException {
		//Workbook
		
		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+fileName+".xlsx");
		
		//Worksheet
		
		XSSFSheet sheet = wbook.getSheetAt(0);
		int rowCount = sheet.getLastRowNum();
		System.out.println("rowcount="+rowCount);
		int columnCount = sheet.getRow(0).getLastCellNum();
		System.out.println("colcount="+columnCount);
		Object[][] data =new Object[rowCount][columnCount];
		for(int i=1;i<=rowCount;i++) {
			//Rows
			XSSFRow row = sheet.getRow(i);
		for(int j=0;j<columnCount;j++) {
			//Columns
			XSSFCell cell = row.getCell(j);
			data[i-1][j]=cell.getStringCellValue();
			//System.out.println(cellValue);
		}
		}
		return data;
	}

}
