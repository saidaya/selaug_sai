package testng;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.ReadExcel;
import wdMethods.ProjectMethods;

public class TC_002_CreateLead extends ProjectMethods{
	@BeforeTest(groups= {"common"})
	public void setdata() {
		testCaseName ="TC_002_CreateLead";
		testCaseDesc="Create a new Lead";
		author = "Sai";
		category = "Smoke";
		excelFileName ="CreateLeadData";
	}

	@Test (groups= {"smoke"},dataProvider="fetchData")
public void createLead(String cName,String fName,String LName) throws InterruptedException {	
	
		WebElement eleCL = locateElement("linkText","Create Lead");
		click(eleCL);
		WebElement eleCompany = locateElement("id","createLeadForm_companyName");
		type(eleCompany,cName);
		WebElement eleFirst = locateElement("id","createLeadForm_firstName");
		type(eleFirst,fName);
		WebElement eleLast = locateElement("id","createLeadForm_lastName");
		type(eleLast,LName);
		WebElement eleSource = locateElement("name","dataSourceId");
		selectDropDownUsingText(eleSource, "Employee");
		WebElement eleMarket = locateElement("name","marketingCampaignId");
		selectDropDownUsingIndex(eleMarket, 2);
		WebElement eleCLb = locateElement("name","submitButton");
		//selectDropDownUsingIndex(eleMarket, 2);
		click(eleCLb);
		//closeBrowser();
		
		
	}
	
	
		
	//public Object[][] fetchData() {
		//Object[][] data =new Object[2][3];
		
		//data[0][0]="testLeaf";
		//data[0][1]="test1";
		//data[0][2]="A";
		
		//data[1][0]="tuna";
		//data[1][1]="test2";
		//data[1][2]="B";
		
		//return data;	
	}



