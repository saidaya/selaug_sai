package testng;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC_003_DeleteLead extends ProjectMethods{
	@BeforeTest(groups= {"common"})
	public void setdata() {
		testCaseName ="TC_003_DeleteLead";
		testCaseDesc="Delete a new Lead";
		author = "Sai";
		category = "Smoke";
	}
	
	@Test(groups= {"sanity"},dataProvider = "positive")
	public void deleteLead(String ccode,String pno) throws InterruptedException {	
			/*startApp("chrome","http://leaftaps.com/opentaps/control/main");
			WebElement eleUN = locateElement("id","username");
			type(eleUN,"DemoSalesManager");
			WebElement elePW = locateElement("id","password");
			type(elePW,"crmsfa");
			Thread.sleep(3000);
			WebElement eleLogin = locateElement("class", "decorativeSubmit");
			click(eleLogin);
			WebElement eleCRM = locateElement("linktext","CRM/SFA");
			click(eleCRM);*/
			
		WebElement eleCL = locateElement("linkText","Create Lead");
			click(eleCL);
			WebElement eleFL = locateElement("linkText","Find Leads");
			click(eleFL);
			WebElement elePhone = locateElement("xpath","//span[text()='Phone']");
			click(elePhone);
			WebElement elep1 = locateElement("name","phoneCountryCode");
			type(elep1,ccode);
			WebElement elepn = locateElement("name","phoneNumber");
			type(elepn,pno);
			WebElement eleFlb = locateElement("xpath","//button[text()='Find Leads']");
			click(eleFlb);
			WebElement table = driver.findElementByXPath("(//table[@cellspacing='0'])[20]");
			List<WebElement> row = driver.findElementsByTagName("tr");
			WebElement firstrow = row.get(1);
			List<WebElement>tabledata = firstrow.findElements(By.xpath("(//table[@cellspacing='0']//a)[33]"));
			WebElement td = tabledata.get(0);
			String text = td.getText();
			System.out.println(text);
			td.click();
			WebElement delete = locateElement("linkText","Delete");
			click(delete);

}
	@DataProvider(name="positive")
	public Object[][] fetchData() {
		Object[][] data =new Object[1][2];
		
		data[0][0]="23";
		data[0][1]="9573259657";
		return data;
}
}
