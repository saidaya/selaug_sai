package testng;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC_003_EditLead extends ProjectMethods{
	
	@BeforeTest(groups= {"common"})
	public void setdata() {
		testCaseName ="TC_003_EditLead";
		testCaseDesc="Edit Lead";
		author = "Sai";
		category = "Smoke";
	}
	
	@Test(groups= {"regression"},dataProvider="positive")
	public void editLead(String fName) throws InterruptedException {	
			WebElement eleCL = locateElement("linkText","Create Lead");
			click(eleCL);
			WebElement eleFL = locateElement("linkText","Find Leads");
			click(eleFL);
			WebElement elePhone = locateElement("xpath","(//input[@name='firstName'])[3]");
			elePhone.sendKeys(fName);
			WebElement eleFlb = locateElement("xpath","//button[text()='Find Leads']");
			click(eleFlb);
			Thread.sleep(3000);
			WebElement table = driver.findElementByXPath("(//table[@cellspacing='0'])[20]");
			List<WebElement> row = driver.findElementsByTagName("tr");
			WebElement firstrow = row.get(1);
			List<WebElement>tabledata = firstrow.findElements(By.xpath("(//table[@cellspacing='0']//a)[33]"));
			WebElement td = tabledata.get(0);
			String text = td.getText();
			System.out.println(text);
			td.click();
			WebElement edit = locateElement("linkText","Edit");
			click(edit);
			WebElement eleMarket = locateElement("name","ownershipEnumId");
			selectDropDownUsingIndex(eleMarket, 2);
			WebElement update = locateElement("xpath","//input[@value='Update']");
			click(update);

}

	@DataProvider(name="positive")
	public Object[][] fetchData() {
		Object[][] data =new Object[2][1];
		
		data[0][0]="Pradeep";
		//data[0][1]="test1";
		//data[0][2]="A";
		
		data[1][0]="Balaji";
		//data[1][1]="test2";
		//data[1][2]="B";
		
		return data;	
	}
	
	@DataProvider(name="negative")
	public Object[][] fetchNegData() {
		Object[][] data =new Object[2][1];
		
		data[0][0]="testLeaf";
		//data[0][1]="test1";
		//data[0][2]="A";
		
		data[1][0]="tuna";
		//data[1][1]="test2";
		//data[1][2]="B";
		
		return data;	
	}
}

