package flipkartTC;

import org.testng.annotations.Test;

import flipkartPages.LoginFK;
import wdMethods.FkMethods;

public class FlipkartTC extends FkMethods {
	
	@Test
	public void TC_001() {
		
		new LoginFK()
		.searchItem()
		.clickSbtn()
		.selectItem()
		.windowHandle()
		.addCart()
		.takeSS();
		
	}

}
