/*package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	public ChromeDriver driver;
	@Given("Open chrome Browser")
	public void openBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 driver = new ChromeDriver();
	}
	
	@And("maximize the browser")
	public void maxBrowser() {
		driver.manage().window().maximize();
	}
	
	@And("Set Timeout")
	public void setTimeout() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@And("Enter Url")
	public void getURL() {
		driver.get("http://leaftaps.com/opentaps");
	}
	@And("Enter UserName as (.*)")
	public void enterUN(String uname) {
		driver.findElementById("username").sendKeys(uname);
	}
	@And("Enter Password as (.*)")
	public void enterPW(String pw) {
		driver.findElementById("password").sendKeys(pw);
	}
	@And("Click Login")
	public void clickSubmit() throws InterruptedException {
		Thread.sleep(3000);
		driver.findElementByClassName("decorativeSubmit").click();
	}
	@And("click on CRMSFA")
	public void clickCRM() {
		driver.findElementByLinkText("CRM/SFA").click();
	}
	@And("click Create Lead")
	public void clickCL() {
		driver.findElementByLinkText("Create Lead").click();
	}
	@And("Enter Company Name as (.*)")
	public void enterCompName(String cname) {
		driver.findElementById("createLeadForm_companyName").sendKeys(cname);
	}
	@And("Enter First Name as (.*)")
	public void enterFName(String fname) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);
	}
	@And("Enter Last Name as (.*)")
	public void enterLName(String lname) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lname);
	}
	@And("Select Source")
	public void selectSource() {
		WebElement src = driver.findElementByName("dataSourceId");
		Select dd = new Select (src);
		dd.selectByIndex(2);
	}
	
	@When("Click Create Lead button")
	public void clickCLB() {
		driver.findElementByName("submitButton").click();
	}
	@Then("verify Lead created.")
	public void verifyCL() {
		System.out.println("Lead is Cretaed");
	}
	
	
	
}
*/