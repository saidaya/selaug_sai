Feature: Create a Lead
#Background:
#
#Given Open chrome Browser     
#And maximize the browser
#And Set Timeout
#And Enter Url  

@Positive
Scenario Outline: Positive Login flow
     
And Enter UserName as DemoSalesManager
And Enter Password as crmsfa
And Click Login
And click on CRMSFA
And Click on Leads
And click Create Lead
And Enter Company Name as <compName>
And Enter First Name as <firstName>
And Enter Last Name as <lastName>
And Select Source
When Click Create Lead button
Then verify Lead created.

Examples:
|compName||firstName||lastName|
|leaftap1||test1||name|
#|leaftap2||test2||name|

 @Negative
Scenario: Negative Login flow
     
And Enter UserName as dsr
And Enter Password as crmsfa
And Click Login
And click on CRMSFA   


